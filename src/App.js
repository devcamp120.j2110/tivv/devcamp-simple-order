import 'bootstrap/dist/css/bootstrap.min.css'
import { Container } from 'reactstrap'
import SimpleOrder from './components/simple-order';
import './App.css'

function App() {
  return (
    <Container className='bg-light'>
      <SimpleOrder/>
    </Container>
  );
}

export default App;
