import { useEffect, useState } from "react";
import { Label, Row } from "reactstrap";
import Iphone from "./order-detail/iphone";
import Nokia from "./order-detail/nokia";
import Samsung from "./order-detail/samsung";


function SimpleOrder() {
    
    //Iphone
    const [iphonePrice, setIphonePrice] = useState(0);
    const getPriceIphone = (val) => {
        setIphonePrice(val);
    }

    //samsung
    const [samsungPrice, setSamsungPrice] = useState(0);
    const getPriceSamsung = (val) => {
        setSamsungPrice(val);
    }
    //Nokia
    const [nokiaPrice, setNokiaPrice] = useState(0);
    const getPriceNokia = (val) => {
        setNokiaPrice(val);
    }
    const [totalprice, setTotalPrice] = useState(0);
    useEffect(() => {
        setTotalPrice(iphonePrice+samsungPrice + nokiaPrice);
    }, [iphonePrice, samsungPrice, nokiaPrice])
    
    return (
        <div className="pt-3">
            <Label className="h3">Complex Example (Event, List, Props, State)</Label>
            <Row className='border border-primary p-3 w-75'>
                <Iphone priceIphone={getPriceIphone}/>
                <Samsung priceSamsung={getPriceSamsung}/>
                <Nokia priceNokia={getPriceNokia}/>           
                <Label>Total: <span>{totalprice}</span> USD</Label>
            </Row>
        </div>
    )
}
export default SimpleOrder;