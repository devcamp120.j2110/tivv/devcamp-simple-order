import { Label, Col, Button } from "reactstrap";
import { useState, useEffect } from "react";
function Samsung({priceSamsung }) {
    const [price, setPrice] = useState(800);
    const [amount, setAmount] = useState(1);
    const onBtnClick = () => {
        setAmount(amount+1);
    }
    useEffect(()=> {
        setPrice(800*amount);
        priceSamsung(price);
        console.log("Produce name: Samsung S9");
        console.log("Amount: " + amount);
        console.log("Price: " + price);
    },[amount, price]);
    return (
        <Col xs={4} className='border border-warning p-2 mx-2'>
            <Label className="h4">Samsung S9</Label><br />
            <Label>Price: <span>{price}</span> USD</Label><br />
            <Label>Quantity: <span>{amount}</span></Label><br />
            <Button onClick={onBtnClick}>+</Button>
        </Col>
    )
}
export default Samsung;