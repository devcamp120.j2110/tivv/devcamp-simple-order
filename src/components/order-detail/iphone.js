import { useEffect, useState } from "react";
import { Label, Col, Button } from "reactstrap";
function Iphone({priceIphone }) {
    const [price, setPrice] = useState(900);
    const [amount, setAmount] = useState(1);
    const onBtnClick = () => {
        setAmount(amount+1);
    }
    useEffect(()=> {
        setPrice(900*amount);
        priceIphone(price);
        console.log("Produce name: Iphone X");
        console.log("Amount: " + amount);
        console.log("Price: " + price);
    },[amount, price]);

    return (
        <Col xs={4} className='border border-warning p-2'>
        <Label className="h4">Iphone X</Label><br />
        <Label>Price: <span>{price}</span> USD</Label><br />
        <Label>Quantity: <span>{amount}</span></Label><br />
        <Button onClick={onBtnClick}>+</Button>
    </Col>
    )
}
export default Iphone;