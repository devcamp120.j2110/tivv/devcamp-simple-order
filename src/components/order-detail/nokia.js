import { Label, Col, Button } from "reactstrap";
import { useState, useEffect } from "react";
function Nokia({priceNokia}) {
    const [price, setPrice] = useState(650);
    const [amount, setAmount] = useState(1);
    const onBtnClick = () => {
        setAmount(amount+1);
        
    }
    useEffect(()=> {
        setPrice(650*amount);
        priceNokia(price);
        console.log("Produce name: Nokia 8");
        console.log("Amount: " + amount);
        console.log("Price: " + price);
    },[amount, price]);
    return (
        <Col xs={4} className='border border-warning p-2'>
            <Label className="h4">Nokia 8</Label><br />
            <Label>Price: <span>{price}</span> USD</Label><br />
            <Label>Quantity: <span>{amount}</span></Label><br />
            <Button onClick={onBtnClick}>+</Button>
        </Col>
    )
}
export default Nokia;